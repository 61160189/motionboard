﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace motionboard.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Dashboard()
        {
            ViewBag.Message = "Your Dashboard page.";

            return View("~/Views/dashboard/Dashboard.cshtml");
        }

    }
    public class mockData
    {
        public int id { get; set; }
        public int machineId { get; set; }
        public DateTime starDate { get; set; }
        public DateTime endDate { get; set; }
        public String status { get; set; }


    }
}